-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2019 at 03:41 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpp_jayapura`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun`
--

CREATE TABLE `tb_akun` (
  `id_akun` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` enum('1','2','3') NOT NULL,
  `delete_status` enum('1','2') NOT NULL,
  `tgl_daftar` date NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akun`
--

INSERT INTO `tb_akun` (`id_akun`, `username`, `password`, `level`, `delete_status`, `tgl_daftar`, `last_login`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '1', '2019-05-06', '2019-08-04 15:13:04'),
(2, 'kepdin', '21232f297a57a5a743894a0e4a801fc3', '2', '1', '2019-05-06', '2019-05-16 11:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_berita`
--

CREATE TABLE `tb_berita` (
  `id_berita` int(11) NOT NULL,
  `judul` text NOT NULL,
  `image` text NOT NULL,
  `isi` text NOT NULL,
  `waktu_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_distrik`
--

CREATE TABLE `tb_distrik` (
  `id_distrik` int(3) NOT NULL,
  `nama_distrik` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_distrik`
--

INSERT INTO `tb_distrik` (`id_distrik`, `nama_distrik`) VALUES
(1, 'Wamena'),
(2, 'Papua');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_kasus`
--

CREATE TABLE `tb_jenis_kasus` (
  `id_jumlah_kasus_tindak_lanjut` int(3) NOT NULL,
  `jenis_kasus_tindak_lanjut` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_kasus`
--

INSERT INTO `tb_jenis_kasus` (`id_jumlah_kasus_tindak_lanjut`, `jenis_kasus_tindak_lanjut`) VALUES
(3, 'KDRTz'),
(4, 'Pemerkosaan'),
(5, 'Pencabulan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_masyarakat`
--

CREATE TABLE `tb_masyarakat` (
  `id_masyarakat` int(11) NOT NULL,
  `no_pengenal` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `delete_status` enum('1','2') NOT NULL,
  `tgl_daftar` date NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_masyarakat`
--

INSERT INTO `tb_masyarakat` (`id_masyarakat`, `no_pengenal`, `password`, `nama`, `alamat`, `no_hp`, `delete_status`, `tgl_daftar`, `last_login`) VALUES
(5, '019239', '0079fcb602361af76c4fd616d60f9414', 'reza', 'pelita', '081239123', '1', '2019-05-06', '0000-00-00 00:00:00'),
(6, '912392', '0079fcb602361af76c4fd616d60f9414', 'fahreza', 'maulana', '08123123123', '1', '2019-05-06', '0000-00-00 00:00:00'),
(7, '01239123', '0079fcb602361af76c4fd616d60f9414', 'fahreza', 'oke', '08123', '1', '2019-05-06', '0000-00-00 00:00:00'),
(8, '0007377869', '21232f297a57a5a743894a0e4a801fc3', 'reza', 'pelita', '08123123123', '1', '2019-05-06', '2019-08-04 15:11:42'),
(9, '123456', '0079fcb602361af76c4fd616d60f9414', 'reza', 'pelita', '081238212', '1', '2019-05-07', '2019-05-08 07:17:57'),
(10, '0123456789', '0079fcb602361af76c4fd616d60f9414', 'oke', 'oke', '01823', '1', '2019-05-08', '0000-00-00 00:00:00'),
(11, '000737281239', '21232f297a57a5a743894a0e4a801fc3', 'fahreza', 'pelita', '085816504185', '1', '2019-07-05', '0000-00-00 00:00:00'),
(12, '10231230', '21232f297a57a5a743894a0e4a801fc3', 'reza', 'pelita', '085816504185', '1', '2019-07-05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengaduan`
--

CREATE TABLE `tb_pengaduan` (
  `id_pengaduan` int(11) NOT NULL,
  `id_pengadu` int(11) NOT NULL,
  `tgl_pengaduan` date NOT NULL,
  `nama_pengadu` varchar(50) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `alamat_pengadu` text NOT NULL,
  `id_distrik_pengadu` int(4) NOT NULL,
  `nomor_hp` varchar(12) NOT NULL,
  `tgl_kejadian` date NOT NULL,
  `nama_korban` varchar(50) NOT NULL,
  `jenis_kelamin_korban` enum('L','P') NOT NULL,
  `id_jenis_kasus` int(3) NOT NULL,
  `alamat_kejadian` text NOT NULL,
  `id_distrik_korban` int(4) NOT NULL,
  `image_ktp` text NOT NULL,
  `isi_pengaduan` text NOT NULL,
  `id_tindak` int(11) NOT NULL,
  `nama_penangan` varchar(50) NOT NULL,
  `hasil_penanganan` text NOT NULL,
  `status_kasus` enum('1','2') NOT NULL,
  `tgl_tindak` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile_dinas`
--

CREATE TABLE `tb_profile_dinas` (
  `id_profile` int(2) NOT NULL,
  `judul_profile` text NOT NULL,
  `isi_profile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tindak_lanjut`
--

CREATE TABLE `tb_tindak_lanjut` (
  `id_tindak` int(11) NOT NULL,
  `tindak_lanjut` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tindak_lanjut`
--

INSERT INTO `tb_tindak_lanjut` (`id_tindak`, `tindak_lanjut`) VALUES
(22, 'Bank Mandiri'),
(24, 'oke');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_akun`
--
ALTER TABLE `tb_akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `tb_berita`
--
ALTER TABLE `tb_berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `tb_distrik`
--
ALTER TABLE `tb_distrik`
  ADD PRIMARY KEY (`id_distrik`);

--
-- Indexes for table `tb_jenis_kasus`
--
ALTER TABLE `tb_jenis_kasus`
  ADD PRIMARY KEY (`id_jumlah_kasus_tindak_lanjut`);

--
-- Indexes for table `tb_masyarakat`
--
ALTER TABLE `tb_masyarakat`
  ADD PRIMARY KEY (`id_masyarakat`);

--
-- Indexes for table `tb_pengaduan`
--
ALTER TABLE `tb_pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `tb_profile_dinas`
--
ALTER TABLE `tb_profile_dinas`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `tb_tindak_lanjut`
--
ALTER TABLE `tb_tindak_lanjut`
  ADD PRIMARY KEY (`id_tindak`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_akun`
--
ALTER TABLE `tb_akun`
  MODIFY `id_akun` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_berita`
--
ALTER TABLE `tb_berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_distrik`
--
ALTER TABLE `tb_distrik`
  MODIFY `id_distrik` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_jenis_kasus`
--
ALTER TABLE `tb_jenis_kasus`
  MODIFY `id_jumlah_kasus_tindak_lanjut` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_masyarakat`
--
ALTER TABLE `tb_masyarakat`
  MODIFY `id_masyarakat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_pengaduan`
--
ALTER TABLE `tb_pengaduan`
  MODIFY `id_pengaduan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_profile_dinas`
--
ALTER TABLE `tb_profile_dinas`
  MODIFY `id_profile` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_tindak_lanjut`
--
ALTER TABLE `tb_tindak_lanjut`
  MODIFY `id_tindak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
