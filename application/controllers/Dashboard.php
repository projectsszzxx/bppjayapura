<?php 
	/**
	 * 
	 */
	class dashboard extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('Lapor_model');
			$this->load->model('Profile_model');
		}
		function index(){
			$data['judul'] = "Dashboard";
			$kasus = 1;
			$data['data']=$this->Lapor_model->getPengaduan($kasus);
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/data-kasus',$data);
			$this->load->view('Templates/footer-admin');
		}
		function berita($aksi=null , $id=''){
			$data['judul'] = "Berita";
			if ($aksi=="delete") {
				$this->Lapor_model->hapusBerita($id);
				redirect('Dashboard/berita');
			}else if ($aksi=="edit") {
				
				$this->form_validation->set_rules('judul','Judul','required');
				$this->form_validation->set_rules('isi','Isi','required');
				if ($this->form_validation->run()==FALSE) {
					$data['data'] = $this->Lapor_model->getBeritaOne($id);
					$this->load->view('Templates/header-admin',$data);
					$this->load->view('Admin/edit-berita',$data);
					$this->load->view('Templates/footer-admin');
				}else{
						$config['upload_path'] = './img/news/';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']     = 2048;
						$config['file_name']     = $_FILES['image']['name'];
						$this->load->library('upload',$config);
						if ($this->upload->do_upload('image')){
							$data = array(
								'id_berita' => $id,
								'judul' => $this->input->post('judul'),
								'image' => $this->upload->data('file_name'),
								'isi' => $this->input->post('isi')
							);
							$this->Lapor_model->editBerita($data);
							redirect('Dashboard/berita');
						}
				}
			}else{
				$data['data'] = $this->Lapor_model->getBerita();
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/berita');
				$this->load->view('Templates/footer-admin');
			}
		}
		function tambah_berita(){
			$data['judul'] = "Tambah Berita";

				$this->form_validation->set_rules('judul','Judul','required');
				$this->form_validation->set_rules('isi','Isi','required');
			if ($this->form_validation->run()==FALSE) {
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/tambah-berita');
				$this->load->view('Templates/footer-admin');
			}else{
					$config['upload_path'] = './img/news/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']     = 2048;
					$config['file_name']     = $_FILES['image']['name'];
					$this->load->library('upload',$config);
					if ($this->upload->do_upload('image')){
						$this->Lapor_model->inputBerita();
						redirect('Dashboard/berita');
					}
			}
		}
		function dataKasus($aksi=''){
			$data['judul'] = "Data Kasus";
			if ($aksi=="cetak") {
				$data['data']=$this->Lapor_model->getPengaduanAwal($kasus);
			}else{
				$kasus = 1;
				$data['data']=$this->Lapor_model->getPengaduan($kasus);
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/data-kasus',$data);
				$this->load->view('Templates/footer-admin');
			}
		}
		function kasusTertangani($aksi='' , $id=''){
			if ($aksi=="detail") {
				$data['judul'] = "Detail Kasus Sukses";
				$data['data']=$this->Lapor_model->getPengaduanOne($id);
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/detail-kasus-sukses');
				$this->load->view('Templates/footer-admin');
			}else if($aksi=="cetak"){
		    	ob_start();
				$data['data']=$this->Lapor_model->getPengaduanOne($id);
			    $this->load->view('Admin/cetak-kasus', $data);
			    $html = ob_get_contents();
			        ob_end_clean();
			        
			        require_once('./assets/html2pdf/html2pdf.class.php');
			    $pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15',array(6, 5, 3, 5)); 
			    $pdf->WriteHTML($html);
			    $pdf->Output('kasus-'.$id.'.pdf', 'D');
			}else if($aksi=="cetak-kekerasan"){
		    	ob_start();
				$data['data']=$this->Lapor_model->getPengaduanOne($id);
				$data['kasus']=$this->Lapor_model->getPengaduanKasus($data['data']['id_jenis_kasus']);
			    $this->load->view('Admin/cetak-kekerasan', $data);
			    $html = ob_get_contents();
			        ob_end_clean();
			        
			        require_once('./assets/html2pdf/html2pdf.class.php');
			    $pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15',array(10, 5, 0, 5)); 
			    $pdf->WriteHTML($html);
			    $pdf->Output('kekerasan-'.$id.'.pdf', 'D');
			}else if($aksi=="cetak-report"){
		    	ob_start();
				$data['data']=$this->Lapor_model->getReport();
			    $this->load->view('Admin/cetak-report', $data);
			    $html = ob_get_contents();
			        ob_end_clean();
			        
			        require_once('./assets/html2pdf/html2pdf.class.php');
			    $pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15',array(6, 5, 3, 5)); 
			    $pdf->WriteHTML($html);
			    $pdf->Output('report-'.date('d m Y').'.pdf', 'D');
			}else{
				$data['judul'] = "Data Kasus";
				$kasus = 2;
				$data['data']=$this->Lapor_model->getPengaduan($kasus);
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/kasus-sukses',$data);
				$this->load->view('Templates/footer-admin');
			}
		}
		function jenisKasus(){
			$data['judul'] = "Data Kasus";
			$data['data']=$this->Lapor_model->getJenisKasus();
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/jenis-kasus',$data);
			$this->load->view('Templates/footer-admin');
		}
		function tambahKasus(){
			$data['judul'] = "Tambah Kasus";

				$this->form_validation->set_rules('namkas','Nama Kasus','required');

			if ($this->form_validation->run()==FALSE) {
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/tambah-jenis-kasus');
				$this->load->view('Templates/footer-admin');
			}else{
				$this->Lapor_model->inputKasus();
				redirect('Dashboard/jenisKasus');
			}
		}
		function tindakan($jenis , $id , $aksi=''){
			$data['judul']="Tindakan";

			if ($jenis=="jenis-kasus") {
				if ($aksi=="hapus") {
					$this->Lapor_model->hapusKasus($id);
					redirect('Dashboard/jenisKasus');
				}else{
					$this->form_validation->set_rules('jenKas','Nama Kasus','required');

					if ($this->form_validation->run()==FALSE) {
						$data['data'] = $this->Lapor_model->getJenisKasusOne($id);
						$this->load->view('Templates/header-admin',$data);
						$this->load->view('Admin/edit-jenis-kasus',$data);
						$this->load->view('Templates/footer-admin');
					}else{
						$id_jen = array(
							"id_jenis_kasus" => $id,
							"jenis_kasus" => $_POST['jenKas']
						);
						$this->Lapor_model->editJenisKasus($id_jen);
						redirect('Dashboard/jenisKasus');
					}
				}
				
			}else if($jenis == "tindak-lanjut"){
				$id = array(
					"id_pengaduan" => $id
				);
				$data['data']=$this->Lapor_model->getPengaduanDetail($id);
				$data['tindak']=$this->Lapor_model->getJenisTindak();
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/tindak-kasus',$data);
				$this->load->view('Templates/footer-admin');
			}else{
				$this->Lapor_model->hapusJenisTindak($id);
				redirect('Dashboard/dataKasus');
			}
		}
		function distrik(){
			$data['judul']="List Distrik";
			$data['data']=$this->Lapor_model->getDistrik();
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/distrik',$data);
			$this->load->view('Templates/footer-admin');
		}
		function tindak_distrik($id , $aksi=''){
			$data['judul']="Distrik";
			if ($aksi=="edit") {
					$this->form_validation->set_rules('namdis','Nama Distrik','required');
				if ($this->form_validation->run()==FALSE){
					$data['data']=$this->Lapor_model->getDistrikOne($id);
					$this->load->view('Templates/header-admin',$data);
					$this->load->view('Admin/edit-distrik',$data);
					$this->load->view('Templates/footer-admin');
				}else{
					$data = array(
						'id_distrik' => $id, 
						'nama_distrik' => $_POST['namdis']
					);
					$this->Lapor_model->editDistrik($data);
					redirect('Dashboard/distrik');
				}
			}else{
				$this->Lapor_model->hapusDistrik($id);
				redirect('Dashboard/distrik');
			}
		}
		function tambahDistrik(){
			$data['judul']="Tambah Distrik";

				$this->form_validation->set_rules('namdis','Nama Distrik','required|alpha');

				if ($this->form_validation->run()== FALSE) {
					$this->load->view('Templates/header-admin',$data);
					$this->load->view('Admin/tambah-distrik');
					$this->load->view('Templates/footer-admin');
				}else{
					$this->Lapor_model->inputDistrik();
					redirect('Dashboard/distrik');
				}
		}
		function laporan(){
			$data['judul']="Tindakan";
			
			//Korban
			$data['jumlahPengaduan'] = count($this->Lapor_model->getPengaduanAll());
			$data['jumlahKorban'] = count($this->Lapor_model->getPengaduanKorban());

			//Laporan
			$data['laporan'] = $this->Lapor_model->getLaporanTahunJumlahKasus();
			$data['laporan_2'] = $this->Lapor_model->lanjut();
			$data['laporan_tindak'] = $this->Lapor_model->lanjut_tindak();

			//Cari Laporan Berdasarkan Tahun
			$data['year'] = $this->Lapor_model->getTahunPengaduan();

			if (isset($_POST['tahun'])) {
				$data['lapTah'] = $this->Lapor_model->getLaporanTahun($_POST['tahun']);
				$data['jenkas'] = $this->Lapor_model->getKasus($_POST['tahun']);
				$data['jumlah'] = $this->Lapor_model->getJumlah($_POST['tahun']);
				$data['count'] = count($data['jumlah']);
					// $this->cetak($tahun);
			}

			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/Laporan',$data);
			$this->load->view('Templates/footer-admin');
		}
		function cetak($tahun=''){
		    if (isset($tahun)) {
		    	ob_start();
				$data['lapTah'] = $this->Lapor_model->getLaporanTahun($tahun);
				$data['jenkas'] = $this->Lapor_model->getKasus($tahun);
				$data['jumlah'] = $this->Lapor_model->getJumlah($tahun);
				$data['count'] = count($data['jumlah']);
			    $this->load->view('Admin/cetak-laporan', $data);
			    $html = ob_get_contents();
			        ob_end_clean();
			        
			        require_once('./assets/html2pdf/html2pdf.class.php');
			    /*$pdf = new HTML2PDF('L','A4','en');*/
			    $pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15',array(3, 5, 2, 5)); 
			    $pdf->WriteHTML($html);
			    $pdf->Output('Laporan-tahun-'.$tahun.'.pdf', 'D');
		    }else{
		    	redirect('Dashboard/laporan');
		    	// var_dump($tahun);
		    }

		}
		function pengaduan(){
			$data['judul']="Tindakan";
			$data['kasus'] = $this->Lapor_model->getJenisKasus();
			$data['distrik'] = $this->Lapor_model->getDistrik();
			$data['distrikor'] = $this->Lapor_model->getDistrik();
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/form-pengaduan',$data);
			$this->load->view('Templates/footer-admin');
		}
		function inputPengaduan(){
			$config['upload_path'] = './img/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']     = 2048;
			$config['file_name']     = $_FILES['filektp']['name'];

			$this->load->library('upload',$config);
			if ($this->upload->do_upload('filektp')){
				$this->Lapor_model->laporInputAdmin();
				$this->session->set_flashData('notif_reg_success','Success');
				redirect('Dashboard/pengaduan');
			}else{
				$this->session->set_flashData('notif_reg_success','Gagal');
				redirect('Dashboard/pengaduan');
			}
		}
		function korban(){
			$data['judul']="Korban";
			$data['jenis_kasus']=$this->Lapor_model->getJenisKasus();
			$data['pengaduan']=$this->Lapor_model->getPengaduanAll();
			$data['jumKor'] = $this->Lapor_model->pengaduanJmlKorb();
			$data['count'] = count($data['pengaduan']);
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/korban',$data);
			$this->load->view('Templates/footer-admin',$data);
		}
		function kasuSelesai(){
			$data['judul']="Korban";
			$data['jenis_kasus']=$this->Lapor_model->getJenisKasus();
			$this->load->view('Templates/header-admin',$data);
			$this->load->view('Admin/korban',$data);
			$this->load->view('Templates/footer-admin',$data);
		}
		function jenisTindak(){
			$data['judul'] = "Jenis Tindak Lanjut";
				$data['data'] = $this->Lapor_model->getJenisTindak();
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/jenis-tindak',$data);
				$this->load->view('Templates/footer-admin',$data);
		}
		function jenisTindakan($jenis , $id , $aksi=''){
			$data['judul'] = "Jenis Tindak Lanjut";
			if ($jenis=="jenis-tindak") {
				if ($aksi=="hapus"){
					$this->Lapor_model->hapusTindak($id);
					redirect('Dashboard/jenisTindak');
				}else{
					$this->form_validation->set_rules('tindak_lanjut','Tindak Lanjut','required');

					if ($this->form_validation->run()==FALSE) {
						$data['data'] = $this->Lapor_model->getTindakLanjut($id);
						$this->load->view('Templates/header-admin',$data);
						$this->load->view('Admin/edit-tindak-lanjut',$data);
						$this->load->view('Templates/footer-admin');
					}else{
						$data = array(
							"id_tindak" => $id,
							"tindak_lanjut" => $_POST['tindak_lanjut']
						);
						$this->Lapor_model->editTindakLanjut($data);
						redirect('Dashboard/jenisTindak');
					}
				}
			}
		}
		function tambahTindakLanjut(){
			$data['judul'] = "Tambah Jenis Tindak Lanjut";


				$this->form_validation->set_rules('tinJut','Nama Kasus','required');

			if ($this->form_validation->run()==FALSE) {
				$this->load->view('Templates/header-admin',$data);
				$this->load->view('Admin/tambah-jenis-tindak-lanjut');
				$this->load->view('Templates/footer-admin');
			}else{
				$this->Lapor_model->inputTindakLanjut();
				redirect('Dashboard/jenisTindak');
			}
		}
		function profile(){
			$data['judul']="Profile";
			$data['data'] = $this->Profile_model->getData();

				$this->load->vieW('Templates/header-admin',$data);
				$this->load->vieW('Admin/profile',$data);
				$this->load->vieW('Templates/footer-admin');
		}
		function tindak_profile($id , $aksi=''){
			$data['judul']="Edit Profile";
			if ($aksi=="edit") {
				$this->form_validation->set_rules('judul','Nama Kasus','required');
				$this->form_validation->set_rules('isi','Nama Kasus','required');
				if ($this->form_validation->run()==FALSE){
					$data['data'] = $this->Profile_model->getDataOne($id);
					$this->load->vieW('Templates/header-admin',$data);
					$this->load->vieW('Admin/edit-profile',$data);
					$this->load->vieW('Templates/footer-admin');
				}else{
					$data = array(
						'id_profile' => $id,
						'judul' => $_POST['judul'],
						'isi' => $_POST['isi']
					);
					$this->Profile_model->editProfile($data);
					redirect('Dashboard/profile');
				}
			}else{
				$this->Profile_model->hapusProfile($id);
				redirect('Dashboard/profile');
			}
		}
		function tambahProfile(){
			$data['judul']="Tambah Profile";

				$this->form_validation->set_rules('judul','Judul','required');
				$this->form_validation->set_rules('isi','isi','required');

				if ($this->form_validation->run()== FALSE) {
					$this->load->vieW('Templates/header-admin',$data);
					$this->load->vieW('Admin/form-profile');
					$this->load->vieW('Templates/footer-admin');
				}else{
					$this->Lapor_model->insertProfile();
					redirect('Dashboard/profile');
				}
		}
		function masyarakat(){
			$data['judul']="Data Masyarakat";
			$status = [
				"delete_status" => 1
			];
			$data['data'] = $this->Lapor_model->getMasyarakat($status);
			$this->load->vieW('Templates/header-admin',$data);
			$this->load->vieW('Admin/data-masyarakat',$data);
			$this->load->vieW('Templates/footer-admin');
		}
	}
 ?>