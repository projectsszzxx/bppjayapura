<?php 
	/**
	 * 
	 */
	class Auth extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('Login_model');
			$this->load->model('Lapor_model');
		}
		function index(){
			if (isset($_SESSION)) {
				$data['judul'] = "Login Admin";
			}else{
				$data['judul'] = "Admin";
			}
			$this->form_validation->set_rules('username','username' , 'required');
			$this->form_validation->set_rules('password','password' , 'required');
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Templates/header',$data);
				$this->load->view('Admin/admin');
				$this->load->view('Templates/footer');
			}else{
				$where = array(
					"username" => $this->input->post('username',true) ,
					"password"=> md5($this->input->post('password',true))
				);
				$jml = $this->Login_model->cekUser($where)->num_rows();
				
				if ($jml>0 && $jml<2){
						$data = $this->Login_model->getUser($where);
						$session = array(
							"id_akun" => $data[0]['id_akun'],
							"username" => $data[0]['username'],
							"level" => $data[0]['level']
						);
					if ($data[0]['delete_status']=="1") {
						$this->Login_model->updateLastLog($data[0]['id_akun']);
						$this->session->set_userdata($session);
						redirect('Dashboard');

					}else{
						echo"eew";
					}
				}else{
					echo "akun tidak ada";
				}
			}
		}
		function masyarakat(){
			$where = [
				"no_pengenal" => $this->input->post('ktp'),
				"password" => md5( $this->input->post('password') ),
			];
			$data = $this->Login_model->login_masyarakat($where)->num_rows();
			if ($data>0 && $data<2) {

				$datas = $this->Lapor_model->getMasyarakat($where);
				$session = array(
					"id" => $datas[0]['id_masyarakat'],
					"ktp" => $datas[0]['no_pengenal'],
					"nama" => $datas[0]['nama']
				);
				if ($datas[0]['delete_status']=="1") {
					$this->Lapor_model->updateLastLog($datas[0]['id_masyarakat']);
					$test = $this->session->set_userdata($session);

					redirect('Lapor');

				}else{
					echo "silahkan hub admin";
				}

			}else{
				echo"gagal";
			}
		}
		function daftar(){
			$data['judul'] = "Pengaduan";
			
			// validation
			$this->form_validation->set_rules('ktp','ID Number' , 'required|numeric');
			$this->form_validation->set_rules('pass','Password' , 'required');
			$this->form_validation->set_rules('nama','Name' , 'required|alpha');
			$this->form_validation->set_rules('alamat','Address' , 'required');
			$this->form_validation->set_rules('no_hp','Phone Number' , 'required|numeric');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('Templates/header',$data);
				$this->load->view('Lapor/daftar');
				$this->load->view('Templates/footer');
			}else{
				$this->Login_model->daftarMasyarakat();
				$this->session->set_flashData('notif_reg_success','Success');
				redirect('Lapor');
			}
		}
	}
 ?>