<?php 
	
	class Lapor extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('Lapor_model');
		}
		function index(){
			$data['judul'] = "Pengaduan";
			if (isset($_SESSION['ktp'])) {
				
				$this->form_validation->set_rules('namaku','Nama' , 'required|alpha');
				$this->form_validation->set_rules('alamatku','Alamat' , 'required');
				$this->form_validation->set_rules('hpku','Nomor Hp' , 'required|numeric');
				$this->form_validation->set_rules('tglkejadian','Tanggal Kejadian' , 'required');
				$this->form_validation->set_rules('namkor','Nama Korban' , 'required|alpha');
				$this->form_validation->set_rules('alkor','Alamat Korban' , 'required');
				$this->form_validation->set_rules('tglkejadian','Tanggal Kejadian' , 'required');
				
				if ($this->form_validation->run()==FALSE) {
					$data['kasus'] = $this->Lapor_model->getJenisKasus();
					$data['distrik'] = $this->Lapor_model->getDistrik();
					$data['distrikor'] = $this->Lapor_model->getDistrik();
					$this->load->view('Templates/header',$data);
					$this->load->view('Lapor/pengaduan',$data);
					$this->load->view('Templates/footer');
				}else{

					$config['upload_path'] = './img/uploads/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']     = 2048;
					$config['file_name']     = $_FILES['filektp']['name'];
					$this->load->library('upload',$config);
					if ($this->upload->do_upload('filektp')){
						$this->Lapor_model->laporInput();
						$this->session->set_flashData('notif_reg_success','Success');
						redirect('Lapor');
					}else{
						$this->Lapor_model->laporInput();
						$this->session->set_flashData('notif_reg_success','Success');
						redirect('Lapor');
					}
				}

			}else{
				$this->load->view('Templates/header',$data);
				$this->load->view('Lapor/login');
				$this->load->view('Templates/footer');
			}
		}
		function pengaduan(){
			$config['upload_path'] = './img/uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']     = 2048;
			$config['file_name']     = $_FILES['filektp']['name'];

			$this->load->library('upload',$config);
			if ($this->upload->do_upload('filektp')){
				$this->Lapor_model->laporInput();
				$this->session->set_flashData('notif_reg_success','Success');
				redirect('Lapor');
			}else{
				echo"gagal";
			}

		}
		function tindakan(){
			$data=[
				"id_pengaduan" => $_POST['id_pengaduan'],
				"status_kasus" => 2,
				"id_tindak" => $_POST['tindakan'],
				"nama_penangan" => $_POST['nama_penangan'],
				"hasil_penanganan" => $_POST['hasil'],
				"tgl_tindak" => $_POST['tgl_tindak'],
			];
			$this->Lapor_model->editStatusPengaduan($data);
			redirect('Dashboard/dataKasus');
		}
		function list_pengaduan(){
			$data['judul'] = 'List Pengaduan';
			$data['laporku'] = $this->Lapor_model->laporanku($_SESSION['id']);
			$this->load->view('Templates/header',$data);
			$this->load->view('Lapor/list-pengaduan',$data);
			$this->load->view('Templates/footer');
		}
	}

 ?>