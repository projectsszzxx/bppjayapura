<?php 
	/**
	 * 
	 */
	class Berita extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('Lapor_model','lapor');
		}
		function index(){
				$data['judul'] = "Berita";
				$data['data'] = $this->lapor->getBerita();
				$this->load->view('Templates/header',$data);
				$this->load->view('Berita/berita',$data);
				$this->load->view('Templates/footer');
		}
		function detail($id){
				$data['data'] = $this->lapor->getBeritaOne($id);
				$data['judul'] = $data['data']['judul'];
				$this->load->view('Templates/header',$data);
				$this->load->view('Berita/detail',$data);
				$this->load->view('Templates/footer');
		}
	}
 ?>