<?php 
	/**
	 * 
	 */
	class Profile extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('Profile_model');
		}
		function index(){
			$data['judul'] = "Profile";
			$data['profile_dinas'] = $this->Profile_model->getData();
			$this->load->view('Templates/header',$data);
			$this->load->view('Profile/profile');
			$this->load->view('Templates/footer');
		}
	}
 ?>