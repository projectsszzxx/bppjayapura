<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}
	public function index()
	{
		$data['judul'] = "Home";
		$this->load->view('Templates/header',$data);
		$this->load->view('Home/index');
		$this->load->view('Templates/footer');
	}
}
