<?php 
	
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasus extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Kasus_model');
		$this->load->model('Lapor_model');
	}
	public function index(){
		$data['judul'] = "Kasus";
		$data['data'] = $this->Kasus_model->getData();
		$data['distrik'] = $this->Kasus_model->getDistrik();
		$data['jenkas'] = $this->Kasus_model->jenkas();
		$data['data2'] = $this->Kasus_model->getDataPerempuan();
		$data['distrik2'] = $this->Kasus_model->getDistrik();
		$data['jenkas2'] = $this->Kasus_model->jenkas();
		$data['data3'] = $this->Kasus_model->getDataAnak();
		$data['distrik3'] = $this->Kasus_model->getDistrik();
		$data['jenkas3'] = $this->Kasus_model->jenkas();
		$this->load->view('Templates/header',$data);
		$this->load->view('Kasus/kasus.php',$data);
		$this->load->view('Templates/footer',$data);
	}
}
 ?>