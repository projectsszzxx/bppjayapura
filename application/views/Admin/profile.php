
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?=base_url()?>Dashboard/tambahProfile" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Profile</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Profile</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Judul Profile</th>
                  <th>Isi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody class="text-center">
                	<?php 
                	$no = 0;
                		foreach ($data as $data) {
                			$no++;
                			?>
			                <tr>
			                  <td><?=$no?></td>
			                  <td><?=$data['judul_profile']?></td>
			                  <td><?=$data['isi_profile']?></td>
			                  <td><a href="<?=base_url()?>Dashboard/tindak-profile/<?=$data['id_profile']?>/edit" class="text-success"><i class="fa fa-pencil"></i></a> <a href="<?=base_url()?>Dashboard/tindak-profile/<?=$data['id_profile']?>/hapus" class="text-danger" onclick="return confirm('Apakah anda yakin ingin menghapus??')"><i class="fa fa-times"></i></a></td>
			                </tr>
                			<?php
                		}
                	 ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->