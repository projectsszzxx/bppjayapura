
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kasus Laporan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>No KTP</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No Hp</th>
                </tr>
                </thead>
                <tbody class="text-center">
                	<?php 
                	$no=1;
                		foreach ($data as $data) {
                			?>
                			<tr>
                				<td><?=$no++ ?></td>
                				<td><?=$data['no_pengenal'] ?></td>
                				<td><?=$data['nama'] ?></td>
                				<td><?=$data['alamat'] ?></td>
                				<td><?=$data['no_hp'] ?></td>
                			</tr>
                			<?php
                		}
                	 ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>No KTP</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No Hp</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->