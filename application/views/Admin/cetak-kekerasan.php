              <style type="text/css">

/*                *{
                  margin:0;
                  padding:0;
                }
                th {
                  padding: 16px 8px;
                  text-align: left;
                  }
*/
/*                table {
                  font: 11px/24px Verdana, Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  width: 100%;
                  }*/
                tr.yellow td {
                  border-top: 1px solid #FB7A31;
                  border-bottom: 1px solid #FB7A31;
                  background: #FFC;
                  }
                td {
                  border-bottom: 1px solid #CCC;
                  padding: 16px 8px;
                  }
                  .text-center{
                    text-align: center;
                  }
  /*              td:first-child {
                  width: 190px;
                  }

                td+td {
                  border-left: 1px solid #CCC;
                  text-align: center;
                  }*/
              </style>
              <table style="width: 700px;">
                <tr class="yellow"> 
                  <td class="text-center" style="width: 25%"><img src="<?=base_url()?>img/logo/logo.png" style="width: 80px; text-align: center;"></td>
                  <td class="text-center" style="width: 75%"><h5>DINAS PEMBERDAYAAN PEREMPUAN DAN PERLIDUNGAN ANAK KOTA JAYAPURA</h5></td>
                </tr>
                <tr>
                  <td colspan="2" style="text-align: center; width: 100%;">Laporan Tindak Lanjut</td>
                </tr>
                <tr>
                  <td style="border:none; width: 25%">Nama Korban </td>
                  <td style="border:none; width: 75%">: <?=$data['nama_korban']?></td>
                </tr>
                <tr>
                  <td style="border:none; width: 25%">Tanggal Kejadian</td>
                  <td style="border:none; width: 75%">: <?=$data['tgl_kejadian'] ?></td>
                </tr>
                <tr>
                  <td style="border:none; width: 25%">Jenis Kekerasan</td>
                  <td style="border:none; width: 75%">: <?=$data['jenis_kasus_tindak_lanjut']?></td>
                </tr>
              </table>

              <table style="width: 100px">
                <tr>
                  <th style="width: 25%">No</th>
                  <th style="width: 25%">Tgl Pengaduan</th>
                  <th style="width: 25%">Yg Menangani</th>
                  <th style="width: 25%">Hasil / Keterangan</th>
                </tr>
                  <?php 
                  $no = 1;
                    foreach ($kasus as $case) {
                      ?>
                    <tr>
                        <td style="width: 25%"><?=$no++?></td>
                        <td style="width: 25%"><?=$case['tgl_pengaduan']?></td>
                        <td style="width: 25%"><?=$case['nama_penangan']?></td>
                        <td style="width: 25%"><?=$case['hasil_penanganan']?></td>
                    </tr>
                      <?php
                    }
                   ?>
                  <tr style="border-top: none;">
                    <td colspan="2"></td>
                    <td colspan="2" style="text-align: right;">Tanggal: ........ / ........ / ........ </td>
                  </tr>
                  <tr style="border-top: none;">
                    <td colspan="2" style="padding: 80px; border-top: none; text-align: center;">
                      Mengetahui:
                      <div style="padding-top: 25px;">
                        __________
                      </div>
                    </td>
                    <td colspan="2" style="padding: 80px; border-top: none; text-align: center;">
                      Yang Melapor
                      <div style="padding-top: 25px;">
                        __________
                      </div>
                    </td>
                  </tr>
              </table>