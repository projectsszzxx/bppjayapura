
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <?php 
            if ($this->session->flashData('notif_reg_success')) {
              ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Oops!</h4>
                <?=$this->session->flashData('notif_reg_success');?>
            </div>
              <?php
            }
         ?>
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">General Elements</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?=form_open_multipart('Dashboard/inputPengaduan') ?>
                <div class="form-group">
                  <label>Tanggal Pengaduan</label>
                  <input type="date" class="form-control" name="tgl_pengaduan" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Yang mengadu</label>
                  <input type="text" class="form-control" name="pengadu" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control select2" name="jenkel" style="width: 100%;">
                      <option value="L">Laki - Laki</option>
                      <option value="P">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" class="form-control" name="alamat" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Distrik</label>
                  <select class="form-control select2" name="distrikku" style="width: 100%;">
                      <?php 
                          foreach ($distrik as $data) {
                            ?>
                              <option value="<?=$data['id_distrik']?>"><?=$data['nama_distrik']?></option>
                            <?php
                          }
                       ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nomor Hp</label>
                  <input type="number" class="form-control" name="hpku" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Tanggal Kejadian</label>
                  <input type="date" class="form-control" name="tgl_kejadian" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Nama Korban</label>
                  <input type="text" class="form-control" name="namkor" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Jenis Korban</label>
                  <select class="form-control select2" name="jenkelKor" style="width: 100%;">
                      <option value="P">Perempuan</option>
                      <option value="A">Anak</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Jenis Kasus</label>
                  <select class="form-control select2" name="jenkas" style="width: 100%;">
                      <?php 
                          foreach ($kasus as $data) {
                            ?>
                              <option value="<?=$data['id_jumlah_kasus_tindak_lanjut']?>"><?=$data['jenis_kasus_tindak_lanjut']?></option>
                            <?php
                          }
                       ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" class="form-control" name="alkor" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label>Distrik</label>
                  <select class="form-control select2" name="distrikkor" style="width: 100%;">
                      <?php 
                          foreach ($distrikor as $data) {
                            ?>
                              <option value="<?=$data['id_distrik']?>"><?=$data['nama_distrik']?></option>
                            <?php
                          }
                       ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" name="filektp">
                </div>

                <!-- textarea -->
                <div class="form-group">
                  <label>Hasil Penanganan</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="ketLap"></textarea>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Tindak</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->