              <style type="text/css">

/*                *{
                  margin:0;
                  padding:0;
                }
                table {
                  font: 11px/24px Verdana, Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  }
                th {
                  padding: 16px 8px;
                  text-align: left;
                  }

*/
                tr.yellow td {
                  border-top: 1px solid #FB7A31;
                  border-bottom: 1px solid #FB7A31;
                  background: #FFC;
                  }
                td {
                  border-bottom: 1px solid #CCC;
                  padding: 16px 8px;
                  }
                td:first-child {
                  width: 190px;
                  }

  /*              td+td {
                  border-left: 1px solid #CCC;
                  text-align: center;*/
                  }
              </style>
              <table>
                <tr class="yellow"> 
                  <td><img src="<?=base_url()?>img/logo/logo.png" style="width: 80px; text-align: center;"></td>
                  <td><h5>DINAS PEMBERDAYAAN PEREMPUAN DAN PERLIDUNGAN ANAK KOTA JAYAPURA</h5></td>
                </tr>
                <tr>
                  <td>ID Pengaduan</td>
                  <td><?=$data['id_pengaduan']?></td>
                </tr>
                <tr>
                  <td>Tanggal Pengaduan</td>
                  <td><?=$data['tgl_pengaduan']?></td>
                </tr>
                <tr>
                  <td>Nomor Hp</td>
                  <td><?=$data['nomor_hp']?></td>
                </tr>
                <tr>
                  <td>Distrik</td>
                  <td><?=$data['nama_distrik']?></td>
                </tr>
                <tr>
                  <td>Jenis Kasus</td>
                  <td><?=$data['jenis_kasus_tindak_lanjut']?></td>
                </tr>
                <tr>
                  <td>Tanggal Kejadian</td>
                  <td><?=$data['tgl_kejadian']?></td>
                </tr>
                <tr>
                  <td>Nama Korban</td>
                  <td><?=$data['nama_korban']?></td>
                </tr>
                <tr>
                  <td>Alamat Korban</td>
                  <td><?=$data['alamat_kejadian']?></td>
                </tr>
                <tr>
                  <td>Distrik Korban</td>
                  <td><?=$data['nama_distrik']?></td>
                </tr>
                <tr>
                  <td>Keterangan</td>
                  <td style="padding: 80px;"></td>
                </tr>
                <tr style="border-top: none;">
                  <td></td>
                  <td style="text-align: right;">Tanggal: ........ / ........ / ........ </td>
                </tr>
                <tr style="border-top: none;">
                  <td style="padding: 80px; border-top: none;">
                    Mengetahui:
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                  <td style="padding: 80px; border-top: none; width: 50%;">
                    Yang Melapor
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                </tr>
              </table>