
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        <div class="box">
          <div class="box-header">
            <h3>Detail Kasus <a href="<?=base_url()?>Dashboard/kasusTertangani/cetak/<?=$data['id_pengaduan']?>" class="btn btn-success">PDF</a></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
              <table class="table" style="text-align: center;">
                <tr>
                  <td>ID Pengaduan</td>
                  <td><?=$data['id_pengaduan']?></td>
                </tr>
                <tr>
                  <td>Tanggal Pengaduan</td>
                  <td><?=$data['tgl_pengaduan']?></td>
                </tr>
                <tr>
                  <td>Nomor Hp</td>
                  <td><?=$data['nomor_hp']?></td>
                </tr>
                <tr>
                  <td>Distrik</td>
                  <td><?=$data['nama_distrik']?></td>
                </tr>
                <tr>
                  <td>Jenis Kasus</td>
                  <td><?=$data['jenis_kasus_tindak_lanjut']?></td>
                </tr>
                <tr>
                  <td>Tanggal Kejadian</td>
                  <td><?=$data['tgl_kejadian']?></td>
                </tr>
                <tr>
                  <td>Nama Korban</td>
                  <td><?=$data['nama_korban']?></td>
                </tr>
                <tr>
                  <td>Alamat Korban</td>
                  <td><?=$data['alamat_kejadian']?></td>
                </tr>
                <tr>
                  <td>Distrik Korban</td>
                  <td><?=$data['nama_distrik']?></td>
                </tr>
                <tr>
                  <td>Keterangan</td>
                  <td><?=$data['isi_pengaduan']?></td>
                </tr>
                <tr style="border-top: none;">
                  <td></td>
                  <td>Tanggal: ........ / ........ / ........ </td>
                </tr>
                <tr style="border-top: none;">
                  <td style="padding: 80px; border-top: none;">
                    Mengetahui:
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                  <td style="padding: 80px; border-top: none;">
                    Yang Melapor
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                </tr>
              </table>
          </div>
          <!-- /.box-body -->
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->