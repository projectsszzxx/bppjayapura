
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          <a href="<?=base_url()?>Dashboard/kasusTertangani" class="btn btn-success">Kasus Tertangani</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kasus Laporan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Id Pengaduan</th>
                  <th>Tgl Pengaduan</th>
                  <th>Nama Pelapor</th>
                  <th>Nama Korban</th>
                  <th>Alamat</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody class="text-center">
                	<?php 
                	$no = 0;
                		foreach ($data as $data) {
                			$no++;
                			?>
			                <tr>
			                  <td><?=$no?></td>
			                  <td><?=$data['id_pengaduan']?></td>
			                  <td><?=$data['tgl_pengaduan']?></td>
			                  <td><?=$data['nama_pengadu']?></td>
			                  <td><?=$data['nama_korban']?></td>
			                  <td><?=$data['alamat_kejadian']?></td>
			                  <td><?=$data['isi_pengaduan']?></td>
			                  <td>
                          <a href="<?=base_url()?>Dashboard/kasusTertangani/cetak-kekerasan/<?=$data['id_pengaduan']?>"><i class="fa fa-file"></i></a>
                          <a href="<?=base_url()?>Dashboard/tindakan/tindak-lanjut/<?=$data['id_pengaduan']?>" class="text-success"><i class="fa fa-sign-in"></i></a> 
                          <a href="<?=base_url()?>Dashboard/tindakan/hapus/<?=$data['id_pengaduan']?>" class="text-danger"><i class="fa fa-close"></i></a></td>
			                </tr>
                			<?php
                		}
                	 ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Id Pengaduan</th>
                  <th>Tgl Pengaduan</th>
                  <th>Nama Pelapor</th>
                  <th>Nama Korban</th>
                  <th>Alamat</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->