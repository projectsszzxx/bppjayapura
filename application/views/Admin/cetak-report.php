             <style type="text/css">
                *{
                  margin:0;
                  padding:0;
                }
                table {
                  font: 11px/24px Verdana, Arial, Helvetica, sans-serif;
                  border-collapse: collapse;
                  margin:0 auto;
                  }
                th {
                  padding: 16px 8px;
                  text-align: left;
                  }

                tr.yellow td {
                  border-top: 1px solid #FB7A31;
                  border-bottom: 1px solid #FB7A31;
                  background: #FFC;
                  }

                td {
                  border-bottom: 1px solid #CCC;
                  padding: 16px 8px;
                  }

                td:first-child {
                  width: 190px;
                  }

                td+td {
                  border-left: 1px solid #CCC;
                  text-align: center;
                  }
              </style>
              <table>
                <tr class="yellow"> 
                  <td colspan="3"><img src="<?=base_url()?>img/logo/logo.png" style="width: 80px; margin: 20px; text-align: center;"></td>
                  <td style="padding-right:10px;" colspan="5"><h5>DINAS PEMBERDAYAAN PEREMPUAN DAN PERLIDUNGAN ANAK KOTA JAYAPURA</h5></td>
                  <td style="padding-right:10px;"></td>
                  <td style="padding-right:10px;"></td>
                </tr>
                <tr>
                  <th>#</th>
                  <th>Jenis Kasus</th>
                  <th>Nama Korban</th>
                  <th>Nama Pengadu</th>
                  <th>Tgl Pengaduan</th>
                  <th>Tindak Lanjut</th>
                  <th>Tgl Tindak Lanjut</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                <tr>
                  <?php 
                  $no = 0;
                    foreach ($data as $data) {
                      $no++;
                      ?>
                      <tr>
                        <td><?=$no?></td>
                        <td><?=$data['jenis_kasus_tindak_lanjut']?></td>
                        <td><?=$data['nama_korban']?></td>
                        <td><?=$data['nama_pengadu']?></td>
                        <td><?=$data['tgl_pengaduan']?></td>
                        <td><?=$data['tindak_lanjut']?></td>
                        <td><?=$data['tgl_tindak']?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <?php
                    }
                   ?>
                </tr>
                <tr style="border-top: none;">
                  <td colspan="10" style="text-align: right;">Tanggal: ........ / ........ / ........ </td>
                </tr>
                <tr style="border-top: none;">
                  <td style="padding: 80px; border-top: none;" colspan="5">
                    Mengetahui:
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                  <td style="padding: 80px; border-top: none;" colspan="5">
                    Mengetahui:
                    <div style="padding-top: 25px;">
                      _______________
                    </div>
                  </td>
                </tr>
              </table>