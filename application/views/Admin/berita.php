
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          <a href="<?=base_url()?>Dashboard/tambah-berita" class="btn btn-success">Tambah Berita</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kasus Laporan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Judul</th>
                </tr>
                </thead>
                <tbody class="text-center">
                  <?php 
                      foreach ($data as $dat) {
                        ?>
                        <tr>
                          <td><?=$dat['judul'] ?></td>
                          <td><a href="<?=base_url()?>Dashboard/berita/edit/<?=$dat['id_berita'] ?>"><i class="fa fa-sign-in text-success"></i></a> <a href="<?=base_url()?>Dashboard/berita/delete/<?=$dat['id_berita'] ?>"><i class="fa fa-close text-danger"></i></a></td>
                        </tr>
                        <?php
                      }
                   ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Judul</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->