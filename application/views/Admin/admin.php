<div class="w-50 m-auto pt-5">
	<?php 
		if(validation_errors()){
			?>
			<div class="alert alert-warning alert-dismissible fade show" role="alert">
				<?=validation_errors();?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<?php
		}
	 ?>
	<form method="POST" action="">
	  <div class="form-group">
	    <label for="Username" class="text-white">Username</label>
	    <input type="text" class="form-control" name="username" id="Username">
	  </div>
	  <div class="form-group">
	    <label for="password" class="text-white">Password</label>
	    <input type="password" class="form-control" name="password" id="password">
	  </div>
	  <button type="submit" class="btn btn-primary text-uppercase">login</button>
	</form>
</div>