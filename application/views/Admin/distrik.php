
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?=base_url()?>Dashboard/tambahDistrik" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Distrik</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Distrik</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>#</th>
                <th>Id Distrik</th>
                <th>Distrik</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody class="text-center">
                  <?php 
                      $no=0;
                      foreach ($data as $data) {
                        $no++;
                        ?>
                          <tr>
                            <td><?=$no?></td>
                            <td><?=$data['id_distrik']?></td>
                            <td><?=$data['nama_distrik']?></td>
                            <td><a href="<?=base_url()?>Dashboard/tindak-distrik/<?=$data['id_distrik']?>/edit" class="text-success"><i class="fa fa-pencil"></i></a> <a href="<?=base_url()?>Dashboard/tindak-distrik/<?=$data['id_distrik']?>/hapus" class="text-danger" onclick="return confirm('Apakah anda yakin ingin menghapus??')"><i class="fa fa-times"></i></a></td>
                          </tr>
                        <?php
                      }
                   ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->