
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">General Elements</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?=form_open('Lapor/tindakan') ?>

                <div class="form-group">
                  <label>Id Pengaduan</label>
                  <input type="text" class="form-control" name="id_pengaduan" value="<?=$data['id_pengaduan']?>" readonly>
                </div>
                <!-- text input -->
                <div class="form-group">
                  <label>Tanggal Pengaduan</label>
                  <input type="date" class="form-control" name="tgl_pengaduan" placeholder="Enter ..." value="<?=$data['tgl_pengaduan']?>" readonly>
                </div>
                <div class="form-group">
                  <label>Yang mengadu</label>
                  <input type="text" class="form-control" name="pengadu" placeholder="Enter ..." value="<?=$data['nama_pengadu']?>" readonly>
                </div>

                <!-- textarea -->
                <div class="form-group">
                  <label>Jenis Tindak Lanjut</label>
                  <select class="form-control" name="tindakan">
                    <?php 
                        foreach ($tindak as $tindakan) {
                          ?>
                            <option value="<?=$tindakan['id_tindak']?>"><?=$tindakan['tindak_lanjut']?></option>
                          <?php
                        }
                     ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Nama Penangan</label>
                  <input type="text" class="form-control" name="nama_penangan">
                </div>
                <div class="form-group">
                  <label>Hasil Penanganan</label>
                  <textarea name="hasil" class="form-control"></textarea>
                </div>
                <div class="form-group">
                  <label>Tanggal Tindak</label>
                  <input type="date" class="form-control" name="tgl_tindak">
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Tindak</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->