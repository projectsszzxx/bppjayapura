
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <?php 
              $no=1;
              $test = array_values($jenis_kasus);
                foreach ($jenis_kasus as $data) {
                  ?>
                    <li class="<?=($data['id_jumlah_kasus_tindak_lanjut']==$test[0]['id_jumlah_kasus_tindak_lanjut']) ? 'active':'' ?> id_jenis"  data-id="<?=$data['id_jumlah_kasus_tindak_lanjut']?>"><a href="#tab_<?=$data['id_jumlah_kasus_tindak_lanjut'];?>" data-toggle="tab"><?=$data['jenis_kasus_tindak_lanjut']?></a></li>
                  <?php
                }
               ?>
            </ul>
            <div class="tab-content">
              <?php 
                $noz=0;
                foreach ($jenis_kasus as $tab){
                  $noz++;
              ?>
              <div class="tab-pane <?=($tab['id_jumlah_kasus_tindak_lanjut']==$test[0]['id_jumlah_kasus_tindak_lanjut']) ? 'active':'' ?>" id="tab_<?=$tab['id_jumlah_kasus_tindak_lanjut']?>">
                <table id="example<?=$noz?>" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id Pengaduan</th>
                    <th>Tgl Pengaduan</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Korban</th>
                    <th>Alamat</th>
                    <th>Keterangan</th>
                  </tr>
                  </thead>
                  <tbody class="text-center">
                <?php 
                  foreach ($pengaduan as $kasus) {
                      if ($tab['id_jumlah_kasus_tindak_lanjut'] == $kasus['id_jenis_kasus']){
                      ?>
                                <tr>
                                  <td><?=$kasus['id_pengaduan']?></td>
                                  <td><?=$kasus['tgl_pengaduan']?></td>
                                  <td><?=$kasus['nama_pengadu']?></td>
                                  <td><?=$kasus['nama_korban']?></td>
                                  <td><?=$kasus['alamat_kejadian']?></td>
                                  <td><?=$kasus['isi_pengaduan']?></td>
                                </tr>
                          <?php
                        }
                      }
                   ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th class="text-center">
                    <?php 
                    $a = count($jumKor);
                    for ($i=0; $i < $a ; $i++) { 
                        if ($tab['id_jumlah_kasus_tindak_lanjut'] == $jumKor[$i]['id_jumlah_kasus_tindak_lanjut']) {
                          echo "Total Korban : ".$jumKor[$i]['jumlah'];
                        }
                    }
                      
                    ?>
                    </th>
                    <th>Id Pengaduan</th>
                    <th>Tgl Pengaduan</th>
                    <th>Nama Pelapor</th>
                    <th>Nama Korban</th>
                    <th>Alamat</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <?php } ?>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->