
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-volume-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Jumlah Pengaduan</span>
                <span class="info-box-number"><?=$jumlahPengaduan?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-user-times"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Jumlah Korban</span>
                <span class="info-box-number"><?=$jumlahKorban?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        </div>


          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Data Jenis Kekerasan</a></li>
                <li ><a href="#tab_2" data-toggle="tab">Data PerTahun</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Table With Full Features</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Tahun</th>
                        <th>Jumlah kasus</th>
                        <th>Tindak lanjut</th>
                        <th>Jenis kasus</th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($laporan as $data) {
                        ?>
                        <tr>
                          <td><?= $data['tahun'] ?></td>
                          <td><?= $data['tindak_lanjut'] ?></td>
                          <td><?php 
                            foreach ($laporan_tindak as $laporan) {
                              if ($data['tahun']==$laporan['tahun']) {
                                ?>
                                <?=$laporan['jumlah_kasus'] ?> : <?=$laporan['tindak_lanjut'] ?><br>
                                <?php
                              }
                            }
                           ?></td>
                          <td><?php 
                            foreach ($laporan_2 as $laporan) {
                              if ($data['tahun']==$laporan['tahun']) {
                                ?>
                                <?=$laporan['jenis_kasus_tindak_lanjut'] ?> : <?=$laporan['jumlah'] ?><br>
                                <?php
                              }
                            }
                           ?></td>
                        </tr>
                        <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.box-body -->
                </div>
              </div>
              <div class="tab-pane" id="tab_2">
                  <?php 
                    if (isset($_POST['tahun'])) {
                      ?>
                        <div class="box">
                          <div class="box-header">
                            <h3 class="box-title">Tahun : <?=$_POST['tahun']?></h3>
                            <?php 
                                if (isset($_POST['tahun'])) {
                                  ?> 
                                    <a href="<?=base_url()?>Dashboard/cetak/<?=$_POST['tahun']?>" class="btn btn-info">Cetak PDF</a>
                                  <?php
                                }
                             ?>
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                              <thead>
                              <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">Jenis kasus</th>
                                <th colspan="12" class="text-center">Bulan</th>
                                <th rowspan="2">jumlah</th>
                              </tr>
                              <tr>
                                <th>Januari</th>
                                <th>Februari</th>
                                <th>Maret</th>
                                <th>April</th>
                                <th>Mei</th>
                                <th>Juni</th>
                                <th>Juli</th>
                                <th>Agustus</th>
                                <th>September</th>
                                <th>Oktober</th>
                                <th>Nopember</th>
                                <th>Desember</th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php
                                $no = 1;
                                foreach ($jenkas as $dataKas) {
                                 ?>
                                 <tr>
                                   <td><?= $no++?></td>
                                   <td><?= $dataKas['jenis_kasus_tindak_lanjut']?></td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="January") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="February") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="March") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="April") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="May") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="June") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="July") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="August") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="September") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="October") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="November") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      foreach ($lapTah as $data) {
                                      if ($dataKas['id_jenis_kasus'] == $data['id_jenis_kasus'] && $data['bulan']=="December") {
                                        echo $data['jumlah_kasus'];
                                      }
                                      }
                                    ?>
                                    </td>
                                    <td>
                                      <?php 
                                      for ($i=0; $i < $count ; $i++) { 
                                          if ($dataKas['id_jenis_kasus'] == $jumlah[$i]['id_jenis_kasus']) {
                                            echo $jumlah[$i]['jumlah'];
                                          }
                                      }
                                     ?>
                                     </td>
                                 </tr>
                                <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                      <?php
                    }
                  echo form_open('Dashboard/laporan');?>
                        <div class="form-group">
                          <label>Pilih Tahun</label>
                            <select class="form-control" name="tahun">
                              <?php 
                                  foreach ($year as $year) {
                                    ?>
                                      <option value="<?=$year['tahun']?>"><?=$year['tahun']?></option>
                                    <?php
                                  }
                               ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info">Cari..</button>
                  </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->