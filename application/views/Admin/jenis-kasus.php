
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <a href="<?=base_url()?>Dashboard/tambahKasus" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jenis Kasus</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Jenis Kasus</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Id Jenis Kasus</th>
                  <th>Jenis Kasus</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody class="text-center">
                	<?php 
                	$no = 0;
                		foreach ($data as $data) {
                			$no++;
                			?>
			                <tr>
			                  <td><?=$no?></td>
			                  <td><?=$data['id_jumlah_kasus_tindak_lanjut']?></td>
			                  <td><?=$data['jenis_kasus_tindak_lanjut']?></td>
			                  <td><a href="<?=base_url()?>Dashboard/tindakan/jenis-kasus/<?=$data['id_jumlah_kasus_tindak_lanjut']?>/edit" class="text-success"><i class="fa fa-pencil"></i></a> <a href="<?=base_url()?>Dashboard/tindakan/jenis-kasus/<?=$data['id_jumlah_kasus_tindak_lanjut']?>/hapus" class="text-danger  "><i class="fa fa-times"></i></a></td>
			                </tr>
                			<?php
                		}
                	 ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Id Jenis Kasus</th>
                  <th>Jenis Kasus</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->