
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">

          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">General Elements</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php $dataz = $data['id_tindak']; ?>
              <?=form_open('Dashboard/jenisTindakan/jenis-tindak/'.$dataz.'/edit') ?>
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Kasus</label>
                  <input type="hidden" name="id" value="<?=$data['id_tindak']?>">
                  <input type="text" class="form-control" name="tindak_lanjut" placeholder="Enter ..." value="<?=$data['tindak_lanjut']?>">
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->