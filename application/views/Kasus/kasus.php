<div class="d-flex align-items-center">
	<div class="w-75 m-auto d-block">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
</div>

<div class="d-flex align-items-center mt-5">
	<div class="w-75 m-auto d-block">
		<div id="perempuan" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
</div>

<div class="d-flex align-items-center my-5">
	<div class="w-75 m-auto d-block">
		<div id="anak" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
</div>