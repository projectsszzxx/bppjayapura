<div class="container mt-5">
  <div class="row">
    <div class="col-3">
      <div class="nav flex-column bg-white" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <?php 
        $no = 0;
          $ss = array_values($profile_dinas);
            foreach ($profile_dinas as $profile) {
              ?>
            <a class="nav-link text-uppercase<?php echo ($profile['judul_profile'] == $ss[0]['judul_profile'] ) ? ' active' : '' ?>" id="v-pills-<?= str_replace(' ', "-", $profile['judul_profile'])?>-tab" data-toggle="pill" href="#v-pills-<?=str_replace(' ', "-", $profile['judul_profile']);?>" role="tab" aria-controls="v-pills-<?=str_replace(' ', "-", $profile['judul_profile'])?>" aria-selected="<?php echo ($profile['judul_profile'] == 'test_demo') ? 'true' : 'false' ?>"><?=$profile['judul_profile']?></a>
              <?php
            }
         ?>
      </div>
    </div>
    <div class="col-9">
      <div class="tab-content" id="v-pills-tabContent">
        <?php 
            foreach ($profile_dinas as $profile) {
              ?>
                <div class="tab-pane fade<?php echo ($profile['judul_profile'] == $ss[0]['judul_profile']) ? ' show active' : '' ?>" id="v-pills-<?=str_replace(' ', "-", $profile['judul_profile'])?>" role="tabpanel" aria-labelledby="v-pills-<?=str_replace(' ', "-", $profile['judul_profile'])?>-tab">
                      <h5><?=$profile['judul_profile']?></h5>
                      <p ><?=$profile['isi_profile']?></p>
                </div>
              <?php
            }
         ?>
      </div>
    </div>
  </div>
</div>