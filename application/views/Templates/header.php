<!DOCTYPE html>
<html>
<head>
	<title><?=$judul ?></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/header.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/bootstrap/css/custom.css">
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>img/logo/logo.png">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|Roboto" rel="stylesheet">
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
</head>
<body>
	<div class="test">
		<div class="w-50 m-auto pt-4">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
						<img src="<?=base_url()?>img/logo/logo.png" style="width: 50%;">
					</div>
					<div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-uppercase  d-flex align-items-center">
						<h5 class="font-weight-bold text-white">dinas pemberdayaan perempuan dan perlindungan anak <p>kota jayapura</p></h5>
					</div>
				</div>
			</div>
		</div>
		<div class="w-75 m-auto pt-5">
			<ul class="nav-links font-weight-bold" id="myTopnav">
				<li><a href="<?=base_url()?>">Home</a></li>
				<li><a href="<?=base_url()?>Berita">Berita</a></li>
				<li><a href="<?=base_url()?>Profile">Profil Dinas</a></li>
				<li><a href="<?=base_url()?>Kasus">Data Kasus Kekerasan</a></li>
				<?php
					if (isset($_SESSION['username'])) {
						?>
				        <li><a href="<?=base_url()?>Dashboard">Dashboard</a></li>
						<?php
					}else{
					?>
						<li><a href="<?=base_url()?>Lapor">Laporan</a></li>
					<?php
					}
				?>
				<?php 
					if (isset($_SESSION['ktp'])) {
						?>
							<li><a href="<?=base_url()?>logout">Logout</a></li>
						<?php
					}else{
						if (isset($_SESSION['username'])) {
							?>
							<li><a href="<?=base_url()?>logout">Logout</a></li>
							<?php
						}else{
						?>
							<li><a href="<?=base_url()?>Auth">Login Admin</a></li>
						<?php
						}
					}
				 ?>
				<li class="icon" onclick="responsiveHeader()">&#9776;</li>
			</ul>
		</div>
	</div>