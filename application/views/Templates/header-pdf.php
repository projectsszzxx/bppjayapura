<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/css/AdminLTE.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminLTE/bower_components/select2/dist/css/select2.min.css">
</head>
<body>