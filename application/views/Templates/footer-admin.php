

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/adminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/adminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/adminLTE/js/adminlte.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/adminLTE/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?=base_url()?>assets/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/adminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

  <!-- page script -->
  <script>
    $(function () {

      $('.textarea').wysihtml5();
      
      <?php
        if (isset($count)){
            for ($i = 1; $i <= $count; $i++) {
              ?>
                var a<?=$i?> = $('#example<?=$i?>').DataTable();
              <?php
            }

        }else{
          ?>
          var as = $('table').attr('id');
          $('#'+as).DataTable();
          <?php
        }
      ?>
      $('.select2').select2();
    })
  </script>
</body>
</html>