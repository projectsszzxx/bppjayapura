<script src="<?=base_url()?>assets/bootstrap/js/jquery-3.3.1.min.js"></script>
<script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/bootstrap/js/header.js"></script>
<script type="text/javascript">
	$(function(){
		$('.carousel').carousel({
		  interval: 2000
		});
	    var test = $(".test").height();
	    var height = $(window).height();
	    var total = height - test - 80;
	    $('.content').css("height", total+"px");


	    $(".custom-file-input").on('change',function(){
	    	let filename  = $(this).val().split('\\').pop();
	    	$(this).next('.custom-file-label').addClass('selected').html(filename);
	    });

<?php 
	if ($_SERVER['REQUEST_URI'] == "/bppJayapura/Kasus") { 

		?>
		Highcharts.chart('container', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Data Kekerasan Tahun <?=$data[0]['tahun']?>'
		    },
		    xAxis: {
		        categories: [
		        <?php 
		        	foreach ($distrik as $distrik){
		        		echo json_encode($distrik['nama_distrik']).',';
		        	}
		         ?>
		         ]
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Korban'
		        }
		    },
		    tooltip: {
		        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Orang<br/>',
		        shared: true
		    },
		    plotOptions: {
		        column: {
		            stacking: 'percent'
		        }
		    },
		    series: [
		    <?php


		    	foreach ($jenkas as $jenkas) {
		    		?>
				    {
				        name: '<?=$jenkas['jenis_kasus_tindak_lanjut']?>',
				        data: [
				        		<?php
    								foreach ($data as $dataw) {
    									if ($jenkas['id_jumlah_kasus_tindak_lanjut'] == $dataw['id_jenis_kasus']){
											echo $dataw['jumlah'].',';
    									}
    								}
				        		?>
				        	]
				    },
		    		<?php
		    	}
		     ?>
		    ]
		});

		Highcharts.chart('perempuan', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Data Kekerasan Perempuan'
		    },
		    xAxis: {
		        categories: [
		        <?php 
		        	foreach ($distrik2 as $distrik){
		        		echo json_encode($distrik['nama_distrik']).',';
		        	}
		         ?>
		         ]
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Korban'
		        }
		    },
		    tooltip: {
		        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Orang<br/>',
		        shared: true
		    },
		    plotOptions: {
		        column: {
		            stacking: 'percent'
		        }
		    },
		    series: [
		    <?php


		    	foreach ($jenkas2 as $jenkas) {
		    		?>
				    {
				        name: '<?=$jenkas['jenis_kasus_tindak_lanjut']?>',
				        data: [
				        		<?php
    								foreach ($data2 as $dataw) {
    									if ($jenkas['id_jumlah_kasus_tindak_lanjut'] == $dataw['id_jenis_kasus']){
											echo $dataw['jumlah'].',';
    									}
    								}
				        		?>
				        	]
				    },
		    		<?php
		    	}
		     ?>
		    ]
		});

		Highcharts.chart('anak', {
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: 'Data Kekerasan Abak'
		    },
		    xAxis: {
		        categories: [
		        <?php 
		        	foreach ($distrik3 as $distrik){
		        		echo json_encode($distrik['nama_distrik']).',';
		        	}
		         ?>
		         ]
		    },
		    yAxis: {
		        min: 0,
		        title: {
		            text: 'Korban'
		        }
		    },
		    tooltip: {
		        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> Orang<br/>',
		        shared: true
		    },
		    plotOptions: {
		        column: {
		            stacking: 'percent'
		        }
		    },
		    series: [
		    <?php


		    	foreach ($jenkas3 as $jenkas) {
		    		?>
				    {
				        name: '<?=$jenkas['jenis_kasus_tindak_lanjut']?>',
				        data: [
				        		<?php
    								foreach ($data3 as $dataw) {
    									if ($jenkas['id_jumlah_kasus_tindak_lanjut'] == $dataw['id_jenis_kasus']){
											echo $dataw['jumlah'].',';
    									}
    								}
				        		?>
				        	]
				    },
		    		<?php
		    	}
		     ?>
		    ]
		});


		<?php
	}
	
 ?>

	});



	function responsiveHeader(){
		var x = document.getElementById("myTopnav");
		if (x.className==="nav-links"){
			x.className+="responsive";
		}else{
			x.className="topnav";
		}
	}
</script>
</body>
</html>