<div class="mt-5 text-white text-center">
  <h3>Pendaftaran</h3>
</div>
<div class="w-50 m-auto py-5">
  <?php 
    if(validation_errors()){
      ?>
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <?=validation_errors();?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php
    }
   ?>
  <form class="text-white" method="POST">
    <div class="form-group">
      <label for="no_ktp">Nomor Ktp</label>
      <input type="number" class="form-control" name="ktp" id="no_ktp" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" name="pass" id="password" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="nama">Nama Lengkap</label>
      <input type="text" class="form-control" name="nama" id="nama" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="alamat">Alamat</label>
      <input type="text" class="form-control" name="alamat" id="alamat" autocomplete="off">
    </div>
    <div class="form-group">
      <label for="hp">Nomor Hp</label>
      <input type="number" class="form-control" name="no_hp" id="hp" autocomplete="off">
    </div>
    <button type="submit" class="btn btn-primary text-uppercase">Daftar</button>
    <a href="<?=base_url()?>Lapor" class="btn btn-success float-right text-white text-uppercase">Login</a>
  </form>
</div>