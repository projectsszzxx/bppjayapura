<div class="w-75 m-auto py-5">
	<h1><a href="<?=base_url('lapor')?>" class="btn btn-success">Form Pengaduan</a> Pengaduan Anda</h1>
	<?php 
	    if ($this->session->flashData('notif_reg_success')) {
	      ?>
	      <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <?=$this->session->flashData('notif_reg_success');?>
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <?php
	    }
	 ?>
	 <div class="container mt-5">
	 	<div class="row">
	 		<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nama Korban</th>
			      <th scope="col">Jenis Kelamin</th>
			      <th scope="col">Tanggal Kejadian</th>
			      <th scope="col">Status</th>
			      <?php 
			      		if ($laporku[0]['status_kasus']>1) {
			      			?>
			      <th scope="col">Nama Penangan</th>
			      <th scope="col">Hasil Penanganan</th>
			      			<?php
			      		}
			       ?>
			    </tr>
			  </thead>
			  <tbody>
				 <?php 
				 $no=1;
				 	foreach ($laporku as $data) {
				 		?>
				 		<tr>
				 			<td><?=$no++?></td>
				 			<td><?=$data['nama_korban']?></td>
				 			<td>
				 				<?php 
				 					if ($data['jenis_kelamin']=="L") {
				 						echo "Laki Laki";
				 					}else{
				 						echo "Perempuan";
				 					}
				 				?>
				 			</td>
				 			<td><?=$data['tgl_kejadian']?></td>
				 			<td>
				 				<?php 
				 					if ($data['status_kasus']=="1") {
				 						echo "Sedang Penanganan";
				 					}else{
				 						echo "Selesai";
				 					}
				 				?>
				 			</td>
						      <?php 
						      		if ($data['status_kasus']>1) {
						      			?>
				 			<td><?=$data['nama_penangan']?></td>
				 			<td><?=$data['hasil_penanganan']?></td>
						      			<?php
						      		}
						       ?>
				 		</tr>
				 		<?php
				 	}
				  ?>
			  </tbody>
			</table>
	 	</div>
	 </div>
</div>