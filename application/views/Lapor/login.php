<div class="w-50 m-auto pt-5">
	<?php 
		if(validation_errors()){
			?>
			<div class="alert alert-warning alert-dismissible fade show" role="alert">
				<?=validation_errors();?>
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<?php
		}
	    if ($this->session->flashData('notif_reg_success')) {
	      ?>
	      <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <?=$this->session->flashData('notif_reg_success');?>
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <?php
	    }
	 ?>
	<form method="POST" action="<?=base_url()?>Auth/masyarakat">
	  <div class="form-group">
	    <label for="Username" class="text-white">Nomor Ktp</label>
	    <input type="text" class="form-control" name="ktp" id="Username">
	  </div>
	  <div class="form-group">
	    <label for="password" class="text-white">Password</label>
	    <input type="password" class="form-control" name="password" id="password">
	  </div>
	  <button type="submit" class="btn btn-primary text-uppercase">login</button>
	  <a href="<?=base_url()?>Auth/daftar" class="btn btn-success float-right text-white text-uppercase">Daftar</a>
	</form>
</div>