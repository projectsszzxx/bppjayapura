<div class="w-75 m-auto py-5">
	<h1>Form Pengaduan <a href="<?=base_url('lapor/list-pengaduan')?>" class="btn btn-success">Pengaduan Anda</a></h1>
	<?php 
	    if ($this->session->flashData('notif_reg_success')) {
	      ?>
	      <div class="alert alert-success alert-dismissible fade show" role="alert">
	        <?=$this->session->flashData('notif_reg_success');?>
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <?php
	    }
	 ?>
	<?=form_open_multipart('Lapor');?>
  	<div class="form-row">
  		<div class="col-4">
		    <div class="form-group">
		      <label for="namaku" class="text-white">Nama Pengadu</label>
		      <input type="text" class="form-control" name="namaku" id="namaku" autocomplete="off">
		      <?= form_error('namaku' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  		<div class="col-4">
		    <div class="form-group">
		    	<label class="d-block text-white">Jenis Kelamin</label>
		    		<select class="custom-select" name="jenkelku">
						<option value="L">Laki - Laki</option>
						<option value="P">Perempuan</option>
					</select>
		    </div>
  		</div>
  		<div class="col-4">
		    <div class="form-group">
		      <label for="alamatku" class="text-white">Alamat</label>
		      <input type="text" class="form-control" name="alamatku" id="alamatku" autocomplete="off">
		      <?= form_error('alamatku' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  	</div>
  	<div class="form-row">
  		<div class="col-4">
		    <div class="form-group">
		      <label for="distrikku" class="text-white">Distrik</label>
		    		<select class="custom-select" name="distrikku">
		    			<?php 
		    				foreach ($distrik as $distrik) {
		    					?>
		    						<option value="<?=$distrik['id_distrik']?>"><?=$distrik['nama_distrik']?></option>
		    					<?php
		    				}
		    			 ?>
					</select>
		    </div>
  		</div>
  		<div class="col-8">
		    <div class="form-group">
		      <label for="hpku" class="text-white">Nomor Hp</label>
		      <input type="text" class="form-control" name="hpku" id="hpku" autocomplete="off">
		      <?= form_error('hpku' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  	</div>
  	<div class="form-row">
  		<div class="col-3">
		    <div class="form-group">
		      <label for="tglkejadian" class="text-white">Tanggal Kejadian</label>
		      <input type="date" class="form-control" name="tglkejadian" id="tglkejadian" autocomplete="off">
		      <?= form_error('tglkejadian' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  		<div class="col-5">
		    <div class="form-group">
		      <label for="namkor" class="text-white">Nama Korban</label>
		      <input type="text" class="form-control" name="namkor" id="namkor" autocomplete="off">
		      <?= form_error('namkor' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  		<div class="col-4">
		    <div class="form-group">
		      <label class="text-white">Jenis Kelamin</label>
		    		<select class="custom-select" name="jenkelKor">
						<option value="L">Laki - Laki</option>
						<option value="P">Perempuan</option>
					</select>
		    </div>
  		</div>
  	</div>
  	<div class="form-row">
  		<div class="col-3">
		    <div class="form-group">
		      <label class="text-white">Jenis Kasus</label>
		    		<select class="custom-select" name="jenkas">
		    			<?php 
		    				foreach ($kasus as $kasus) {
		    					?>
		    						<option value="<?=$kasus['id_jumlah_kasus_tindak_lanjut']?>"><?=$kasus['jenis_kasus_tindak_lanjut']?></option>
		    					<?php
		    				}
		    			 ?>
					</select>
		    </div>
  		</div>
  		<div class="col-3">
		    <div class="form-group">
		      <label for="alkor" class="text-white">Alamat Korban</label>
		      <input type="text" class="form-control" name="alkor" id="alkor" autocomplete="off">
		      <?= form_error('alkor' , '<small class="text-white pl-3">','</small>') ?>
		    </div>
  		</div>
  		<div class="col-3">
		    <div class="form-group">
		      <label for="distrikkor" class="text-white">Distrik</label>
		    		<select class="custom-select" name="distrikkor">
		    			<?php 
		    				foreach ($distrikor as $distrik) {
		    					?>
		    						<option value="<?=$distrik['id_distrik']?>"><?=$distrik['nama_distrik']?></option>
		    					<?php
		    				}
		    			 ?>
					</select>
		    </div>
  		</div>
  		<div class="col-3">
		    <div class="form-group">
		      <label for="filektp" class="text-white">Foto Ktp</label>
			  <div class="custom-file">
			    <input type="file" class="custom-file-input" name="filektp" id="filektp" accept=".jpg , .png , .gif">
			    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
		      	<?= form_error('filektp' , '<small class="text-white pl-3">','</small>') ?>
			  </div>
		    </div>
  		</div>
  	</div>
    <div class="form-group">
	    <label for="ketLap" class="text-white">Keterangan Laporan</label>
	    <textarea class="form-control" name="ketLap" id="ketLap" rows="3"></textarea>
		<?= form_error('ketLap' , '<small class="text-white pl-3">','</small>') ?>
    </div>
    	<button type="submit" name="" class="btn-danger btn text-uppercase">Lapor</button>
  </form>
</div>