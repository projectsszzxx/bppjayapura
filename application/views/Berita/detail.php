<div class="container">
	<div class="row">
		<div class="col-12">
			<h1><?=$data['judul']?></h1>
			<div style=" max-height: 450px; overflow: hidden;text-align: center;">
				<img src="<?=base_url()?>/img/news/<?=$data['image']?>" class="img-fluid text-center">
			</div>
			<div class="text-white">
				<?=$data['isi'] ?>
			</div>
		</div>
	</div>
</div>