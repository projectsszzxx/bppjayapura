<div class="d-flex align-items-center content">
<!-- 	<div class="w-50 m-auto text-center text-uppercase">
		<h1 class="font-weight-bold text-white">selamat datang di sistem informasi pemberdayaan kota jayapura</h1>
	</div> -->
	<div class="w-75 m-auto">
		<div class="container-fluid">
			<div class="row">
		<?php 
			foreach ($data as $berita) {
				?>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="card" style="width: 18rem;">
						<div  style="max-height: 300px; overflow: hidden;">
					  		<img class="card-img-top" src="<?=base_url()?>img/news/<?=$berita['image']?>" alt="Card image cap">
						</div>
					  <div class="card-body">
					    <h5 class="card-title"><?=$berita['judul']?></h5>
					    <a href="<?=base_url()?>Berita/detail/<?=$berita['id_berita']?>" class="btn btn-primary">Go somewhere</a>
					  </div>
					</div>
				</div>
				<?php
			}
		 ?>
			</div>
		</div>
	</div>
</div>