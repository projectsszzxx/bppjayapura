<?php 

/**
 * 
 */
class Profile_model extends CI_model
{
	
	function getData()
	{
		return $this->db->get('tb_profile_dinas')->result_array();
	}
	function getDataOne($id)
	{
		return $this->db->get_where('tb_profile_dinas',array('id_profile' => $id))->row_array();
	}
	function editProfile($id){
		$data = [
			'judul_profile' => $id['judul'],
			'isi_profile' => $id['isi']
		];
		$this->db->where('id_profile',$id['id_profile']);
		$this->db->update('tb_profile_dinas',$data);
	}
	function hapusProfile($id){
		$this->db->where('id_profile',$id['id_profile']);
		$this->db->delete('tb_profile_dinas');
	}
}

 ?>