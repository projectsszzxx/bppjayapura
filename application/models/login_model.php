<?php 
	/**
	 * 
	 */
	class Login_model extends CI_model
	{
		private $tb_akun='tb_akun';
		private $tb = 'tb_masyarakat';
		
		function cekUser($where)
		{
			return $this->db->get_where($this->tb_akun , $where);
		}
		function getUser($where){
			return $this->db->get_where($this->tb_akun,$where)->result_array();
		}
		function updateLastLog($id){
			$dateTime = date("Y-m-d H:i:s");
			$data=[
				"last_login" => $dateTime
			];
			$this->db->where('id_akun',$id);
			$this->db->update($this->tb_akun,$data);
		}


		function daftarMasyarakat()
		{
			$data=[
				"no_pengenal" => $this->input->post('ktp',true),
				"password" => md5($this->input->post('pass',true)),
				"nama" => $this->input->post('nama',true),
				"alamat" => $this->input->post('alamat',true),
				"no_hp" => $this->input->post('no_hp',true),
				"tgl_daftar" => date('Y-m-d')
			];
			$this->db->insert($this->tb,$data);
		}
		function login_masyarakat($where){
			return $this->db->get_where($this->tb ,$where);
		}
	}
 ?>