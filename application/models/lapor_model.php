<?php 

	/**
	 * 
	 */
	class Lapor_model extends CI_model
	{
		private $tb = 'tb_masyarakat';
		private $tb_berita = 'tb_berita';
		private $tb_laporan = 'tb_pengaduan';
		private $tb_tindakan = 'tb_tindak_lanjut';
		private $tb_jenkas = 'tb_jenis_kasus';
		private $tb_distrik = 'tb_distrik';
		private $tb_profile = 'tb_profile_dinas';


		//Tambahkan
		function laporInputAdmin(){
			$img = $this->upload->data('file_name');
			$id = $_SESSION['id'];
			$data=[
				"id_pengadu" => $id,
				"tgl_pengaduan" => $this->input->post('tgl_pengaduan'),
				"nama_pengadu" => $this->input->post('pengadu',true),
				"jenis_kelamin" => $this->input->post('jenkel',true),
				"alamat_pengadu" => $this->input->post('alamat',true),
				"id_distrik_pengadu" => $this->input->post('distrikku',true),
				"nomor_hp" => $this->input->post('hpku',true),
				"tgl_kejadian" => $this->input->post('tgl_kejadian',true),
				"nama_korban" => $this->input->post('namkor',true),
				"jenis_kelamin_korban" => $this->input->post('jenkelKor',true),
				"id_jenis_kasus" => $this->input->post('jenkas',true),
				"alamat_kejadian" => $this->input->post('alkor',true),
				"id_distrik_korban" => $this->input->post('distrikkor',true),
				"image_ktp" => $img,
				"isi_pengaduan"  => $this->input->post('ketLap',true)
			];
			$this->db->insert($this->tb_laporan,$data);
		}
		function laporInput(){
			$img = $this->upload->data('file_name');
			$id = $_SESSION['id'];
			$data=[
				"id_pengadu" => $id,
				"tgl_pengaduan" => date('Y-m-d'),
				"nama_pengadu" => $this->input->post('namaku',true),
				"jenis_kelamin" => $this->input->post('jenkelku',true),
				"alamat_pengadu" => $this->input->post('alamatku',true),
				"id_distrik_pengadu" => $this->input->post('distrikku',true),
				"nomor_hp" => $this->input->post('hpku',true),
				"tgl_kejadian" => $this->input->post('tglkejadian',true),
				"nama_korban" => $this->input->post('namkor',true),
				"jenis_kelamin_korban" => $this->input->post('jenkelKor',true),
				"id_jenis_kasus" => $this->input->post('jenkas',true),
				"alamat_kejadian" => $this->input->post('alkor',true),
				"id_distrik_korban" => $this->input->post('distrikkor',true),
				"image_ktp" => $img,
				"isi_pengaduan"  => $this->input->post('ketLap',true)
			];
			$this->db->insert($this->tb_laporan,$data);
		}
		function inputBerita(){
			$data=[
				"judul" => $this->input->post('judul',true),
				"image" => $this->upload->data('file_name'),
				"isi" => $this->input->post('isi',true)
			];
			$this->db->insert($this->tb_berita,$data);
		}
		function inputKasus(){
			$data = [
				"jenis_kasus_tindak_lanjut" => $this->input->post('namkas',true)
			];
			$this->db->insert($this->tb_jenkas,$data);
		}
		function inputDistrik(){
			$data = [
				"nama_distrik" => $this->input->post('namdis',true)
			];
			$this->db->insert($this->tb_distrik,$data);
		}
		function inputTindakLanjut(){
			$data = [
				"tindak_lanjut" => $this->input->post('tinJut',true)
			];
			$this->db->insert($this->tb_tindakan,$data);
		}
		function insertProfile(){
			$data = [
				"judul_profile" => $this->input->post('judul',true),
				"isi_profile" => $this->input->post('isi',true)
			];
			$this->db->insert($this->tb_profile,$data);
		}

		//tampilkan
		function getBerita(){
			return $this->db->get($this->tb_berita)->result_array();
		}
		function getBeritaOne($id){
			return $this->db->get_where($this->tb_berita,array('id_berita' =>$id))->row_array();
		}
		function getTahunPengaduan(){
			$this->db->select('YEAR(tgl_pengaduan) AS tahun');
			$this->db->distinct('tahun');
			$this->db->order_by('tahun','ASC');
			return $this->db->get($this->tb_laporan)->result_array();
		}
		function getLaporanTahun($tahun){
			$this->db->select('MONTHNAME(p.tgl_pengaduan) AS bulan , COUNT(p.id_jenis_kasus) AS jumlah_kasus , p.id_jenis_kasus , YEAR(p.tgl_pengaduan) AS tahun, jk.jenis_kasus_tindak_lanjut');
			$this->db->from($this->tb_laporan . ' p');
			$this->db->join($this->tb_jenkas . ' jk' , 'p.id_jenis_kasus = jk.id_jumlah_kasus_tindak_lanjut');
			$this->db->like('p.tgl_pengaduan' , $tahun , 'both');
			$this->db->group_by(array('MONTH(p.tgl_pengaduan)' , 'p.id_jenis_kasus'));
			$this->db->order_by('MONTH(p.tgl_pengaduan) , DATE(p.tgl_pengaduan) ASC');
			return $this->db->get()->result_array();
		}
		function getKasus($tahun){
			$this->db->select('p.id_jenis_kasus , jk.jenis_kasus_tindak_lanjut , YEAR(p.tgl_pengaduan) AS tahun');
			$this->db->from($this->tb_laporan .' p');
			$this->db->join($this->tb_jenkas .' jk','p.id_jenis_kasus = jk.id_jumlah_kasus_tindak_lanjut');
			$this->db->group_by('p.id_jenis_kasus');
			$this->db->like('p.tgl_pengaduan' , $tahun , 'both');
			return $this->db->get()->result_array();
		}
		function getJumlah($tahun){
			$sql = "SELECT YEAR(p.tgl_pengaduan) AS tahun , COUNT(p.id_jenis_kasus) AS jumlah , p.id_jenis_kasus FROM tb_pengaduan p WHERE YEAR(p.tgl_pengaduan) LIKE $tahun GROUP BY p.id_jenis_kasus";
			return $this->db->query($sql)->result_array();
		}
		function getMasyarakat($where){
			return $this->db->get_where($this->tb ,$where)->result_array();
		}
		function getPengaduan($kasus){
			$this->db->select('*');
			$this->db->from($this->tb_laporan.' lp');
			$this->db->join($this->tb_jenkas.' jk' , 'lp.id_jenis_kasus=jk.id_jumlah_kasus_tindak_lanjut');
			$this->db->where(array('status_kasus'=>$kasus));
			return $this->db->get()->result_array();
		}
		function getReport(){
			$this->db->select('*');
			$this->db->from($this->tb_laporan .' p');
			$this->db->join($this->tb_jenkas .' jk','p.id_jenis_kasus = jk.id_jumlah_kasus_tindak_lanjut','inner');
			$this->db->join($this->tb_tindakan .' tl','p.id_tindak = tl.id_tindak','inner');
			return $this->db->get()->result_array();
		}
		function getPengaduanAll(){
			$this->db->select('j.*, p.*');
			$this->db->from($this->tb_jenkas . ' j');
			$this->db->join($this->tb_laporan . ' p', 'j.id_jumlah_kasus_tindak_lanjut = p.id_jenis_kasus');
			$this->db->order_by('j.id_jumlah_kasus_tindak_lanjut', 'ASC');
			return $this->db->get()->result_array();
		}
		function getPengaduanKasus($id){
			$this->db->select('*');
			$this->db->from($this->tb_laporan .' p');
			$this->db->join($this->tb_jenkas .' jk','p.id_jenis_kasus = jk.id_jumlah_kasus_tindak_lanjut','inner');
			$this->db->join($this->tb_distrik .' d','p.id_distrik_korban = d.id_distrik','inner');
			$this->db->join($this->tb_distrik .' ds','p.id_distrik_pengadu = ds.id_distrik','inner');
			$this->db->where(array('id_jenis_kasus'=>$id));
			return $this->db->get()->result_array();
		}
		function pengaduanJmlKorb(){
			$sql = "SELECT j.* , p.* , COUNT(p.id_jenis_kasus) as jumlah FROM tb_jenis_kasus j INNER JOIN tb_pengaduan p ON j.id_jumlah_kasus_tindak_lanjut = p.id_jenis_kasus GROUP BY p.id_jenis_kasus";
			return $this->db->query($sql)->result_array();
		}
		function getPengaduanKorban(){
			return $this->db->get($this->tb_laporan)->result_array();
		}
		function getPengaduanOne($id){
			$this->db->select('*');
			$this->db->from($this->tb_laporan .' p');
			$this->db->join($this->tb_jenkas .' jk','p.id_jenis_kasus = jk.id_jumlah_kasus_tindak_lanjut','inner');
			$this->db->join($this->tb_distrik .' d','p.id_distrik_korban = d.id_distrik','inner');
			$this->db->join($this->tb_distrik .' ds','p.id_distrik_pengadu = ds.id_distrik','inner');
			$this->db->where(array('id_pengaduan'=>$id));
			return $this->db->get()->row_array();
		}
		function getJenisKasusone($data){
			return $this->db->get_where($this->tb_jenkas,array('id_jumlah_kasus_tindak_lanjut'=>$data) )->row_array();
		}
		function getTindakLanjut($data){
			return $this->db->get_where($this->tb_tindakan,array('id_tindak'=>$data) )->row_array();
		}
		function getJenisKasus(){
			return $this->db->get($this->tb_jenkas)->result_array();
		}
		function getPengaduanDetail($id){
			return $this->db->get_where($this->tb_laporan,$id)->row_array();
		}
		function getDistrik(){
			return $this->db->get($this->tb_distrik)->result_array();
		}
		function getDistrikOne($id){
			return $this->db->get_where($this->tb_distrik,array("id_distrik"=>$id))->row_array();
		}
		function getJumlahKorban(){
			return $this->db->get($this->tb_laporan)->result_array();
		}
		function jumlahKorban($id){
			return $this->db->get_where($this->tb_laporan,array("id_jenis_kasus"=>$id))->result_array();
		}
		function laporanku($id){
			return $this->db->get_where($this->tb_laporan,array('id_pengadu' =>$id))->result_array();
		}
		function getLaporanTahunJumlahKasus() {
			$this->db->select("YEAR(p.tgl_pengaduan) AS tahun, COUNT(p.id_jenis_kasus) AS tindak_lanjut");
			$this->db->from("tb_pengaduan p");
			$this->db->where("status_kasus" , 2);
			$this->db->group_by("YEAR(p.tgl_pengaduan)");
			return $this->db->get()->result_array();
		}
		function lanjut() {
			$sql = "SELECT YEAR(p.tgl_pengaduan) AS tahun, COUNT(j.id_jumlah_kasus_tindak_lanjut) AS jumlah, j.jenis_kasus_tindak_lanjut FROM tb_pengaduan p JOIN tb_jenis_kasus j ON p.id_jenis_kasus = j.id_jumlah_kasus_tindak_lanjut WHERE p.status_kasus = 2 GROUP BY p.id_jenis_kasus, YEAR(p.tgl_pengaduan) ORDER BY YEAR(p.tgl_pengaduan) ASC";
			return $this->db->query($sql)->result_array();
		}
		function lanjut_tindak(){
			$sql ="SELECT YEAR(p.tgl_pengaduan) AS tahun , COUNT(p.id_jenis_kasus) as jumlah_kasus , t.tindak_lanjut FROM tb_pengaduan p INNER JOIN tb_tindak_lanjut t ON p.id_tindak=t.id_tindak WHERE status_kasus = 2 GROUP BY YEAR(p.tgl_pengaduan) , t.id_tindak";
			return $this->db->query($sql)->result_array();
		}
		function getJenisTindak(){
			return $this->db->get($this->tb_tindakan)->result_array();
		}

		//Edit
		function editStatusPengaduan($id){
			$data =[
				"id_tindak" => $id['id_tindak'],
				"nama_penangan" => $id['nama_penangan'],
				"hasil_penanganan" => $id['hasil_penanganan'],
				"status_kasus" => $id['status_kasus'],
				"tgl_tindak" => $id['tgl_tindak'],
			];
			$this->db->where('id_pengaduan',$id['id_pengaduan']);
			$this->db->update($this->tb_laporan,$data);
		}
		function editBerita($data){
			$berita =[
				"judul" => $data['judul'],
				"image" => $data['image'],
				"isi" => $data['isi']
			];
			$this->db->where('id_berita',$data['id_berita']);
			$this->db->update($this->tb_berita,$berita);
		}
		function updateLastLog($id){
			$dateTime = date("Y-m-d H:i:s");
			$data=[
				"last_login" => $dateTime
			];
			$this->db->where('id_masyarakat',$id);
			$this->db->update($this->tb,$data);
		}
		function editJenisKasus($id){
			$data =[
				"jenis_kasus_tindak_lanjut" => $id['jenis_kasus']
			];
			$this->db->where('id_jumlah_kasus_tindak_lanjut',$id['id_jenis_kasus']);
			$this->db->update($this->tb_jenkas,$data);
		}
		function editTindakLanjut($id){
			$data =[
				"tindak_lanjut" =>$id['tindak_lanjut']
			];
			$this->db->where('id_tindak',$id['id_tindak']);
			$this->db->update($this->tb_tindakan,$data);
		}
		function editDistrik($id){
			$data = [
				"nama_distrik" => $id['nama_distrik']
			];
			$this->db->where('id_distrik',$id['id_distrik']);
			$this->db->update($this->tb_distrik,$data);
		}
		//Hapus
		function hapusKasus($id){
			$this->db->where('id_jumlah_kasus_tindak_lanjut',$id);
			$this->db->delete($this->tb_jenkas);
		}
		function hapusTindak($id){
			$this->db->where('id_tindak',$id);
			$this->db->delete($this->tb_tindakan);
		}
		function hapusDistrik($id){
			$this->db->where('id_distrik',$id);
			$this->db->delete($this->tb_distrik);
		}
		function hapusJenisTindak($id){
			$this->db->where('id_pengaduan',$id);
			$this->db->delete($this->tb_laporan);
		}
		function hapusBerita($id){
			$this->db->where('id_berita',$id);
			$this->db->delete($this->tb_berita);
		}
	}

 ?>