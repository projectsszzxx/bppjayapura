<?php 
	/**
	 * 
	 */
	class Kasus_model extends CI_model
	{
		private $tb_pengaduan = 'tb_pengaduan';
		private $tb_jenis_kasus = 'tb_jenis_kasus';
		function getData()
		{
			$sql = "SELECT a.id_jenis_kasus , COUNT(a.id_jenis_kasus) as jumlah , YEAR(a.tgl_pengaduan) as tahun FROM tb_pengaduan as a INNER JOIN tb_jenis_kasus as b ON a.id_jenis_kasus = b.id_jumlah_kasus_tindak_lanjut WHERE YEAR(a.tgl_pengaduan) = YEAR(CURDATE()) GROUP BY a.id_jenis_kasus ,tahun";
			// $sql = "SELECT YEAR(p.tgl_pengaduan) AS tanggal , COUNT(p.id_jenis_kasus) AS jumlah , p.id_jenis_kasus FROM tb_pengaduan p INNER JOIN tb_jenis_kasus jk ON p.id_jenis_kasus = jk.id_jenis_kasus GROUP BY p.id_jenis_kasus , YEAR(p.tgl_pengaduan) ORDER BY YEAR(p.tgl_pengaduan) ASC";
			return $this->db->query($sql)->result_array();
		}
		function getDataPerempuan(){
			$sql = "SELECT a.id_jenis_kasus , COUNT(a.id_jenis_kasus) as jumlah , YEAR(a.tgl_pengaduan) as tahun FROM tb_pengaduan as a INNER JOIN tb_jenis_kasus as b ON a.id_jenis_kasus = b.id_jumlah_kasus_tindak_lanjut WHERE YEAR(a.tgl_pengaduan) BETWEEN YEAR(CURDATE()-4) AND YEAR(CURDATE()) AND a.jenis_kelamin = 'P' GROUP BY a.id_jenis_kasus ,tahun";
			return $this->db->query($sql)->result_array();
		}
		function getDataAnak(){
			$sql = "SELECT a.id_jenis_kasus , COUNT(a.id_jenis_kasus) as jumlah , YEAR(a.tgl_pengaduan) as tahun FROM tb_pengaduan as a INNER JOIN tb_jenis_kasus as b ON a.id_jenis_kasus = b.id_jumlah_kasus_tindak_lanjut WHERE YEAR(a.tgl_pengaduan) BETWEEN YEAR(CURDATE()-4) AND YEAR(CURDATE()) AND a.jenis_kelamin = 'P' GROUP BY a.id_jenis_kasus ,tahun";
			return $this->db->query($sql)->result_array();
		}
		function jenkas()
		{
			return $this->db->get($this->tb_jenis_kasus)->result_array();
		}
		function getDistrik(){
			return $this->db->get('tb_distrik')->result_array();
		}
		function getTahun(){
			$sql = "SELECT DISTINCT(YEAR(p.tgl_pengaduan)) AS tahun FROM tb_pengaduan p ORDER BY YEAR(p.tgl_pengaduan) ASC";
			return $this->db->query($sql)->result_array();
		}
		function getLaporanTahun(){
			$this->db->select('MONTHNAME(p.tgl_pengaduan) AS bulan , COUNT(p.id_jenis_kasus) AS jumlah_kasus , p.id_jenis_kasus , YEAR(p.tgl_pengaduan) AS tahun, jk.jenis_kasus');
			$this->db->from($this->tb_pengaduan . ' p');
			$this->db->join($this->tb_jenis_kasus . ' jk' , 'p.id_jenis_kasus = jk.id_jenis_kasus');
			$this->db->group_by(array('MONTH(p.tgl_pengaduan)' , 'p.id_jenis_kasus'));
			$this->db->order_by('MONTH(p.tgl_pengaduan) , DATE(p.tgl_pengaduan) ASC');
			return $this->db->get()->result_array();
		}
		function getKasus(){
			$this->db->select('p.id_jenis_kasus , jk.jenis_kasus , YEAR(p.tgl_pengaduan) AS tahun');
			$this->db->from($this->tb_pengaduan .' p');
			$this->db->join($this->tb_jenis_kasus .' jk','p.id_jenis_kasus = jk.id_jenis_kasus');
			$this->db->group_by('p.id_jenis_kasus');
			return $this->db->get()->result_array();
		}
	}
 ?>